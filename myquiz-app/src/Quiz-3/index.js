import React, {useContext} from 'react'
import {BrowserRouter as Router, Link, Switch, Route} from "react-router-dom";
import "./public/css/style.css"
import logo from './public/img/logo.png';
import About from './about'
import {LoginContext} from './LoginContext';
import Login from './Login'
import MovieListEditor from './movie/indexMovie'
import {MovieContext, MovieProvider} from './movie/MovieContext';



export default function App() {
    const [login, setLogin, input, setInput] = useContext(LoginContext) 

    return(
        <Router>
            <header>
                <img id="logo" src={logo} width="200px" />
                <nav>
                    <ul>
                        <li>
                            <Link style={{textDecoration: "none"}} to="/">Home</Link>
                        </li>
                        <li>
                            <Link style={{textDecoration: "none"}} to="/about">About</Link>
                        </li>
                        {login.enableEdit&&
                            <li>
                                <Link to="/Movie_Edit">Movie List Editor</Link>
                            </li>
                        }
                        {!login.logout&&
                        <li>
                            <Link to="/Login">Login</Link>
                        </li>
                        }
                        {login.logout&&
                            <li>
                                <Link to="/Logout">Logout</Link>
                            </li>
                        }
                    </ul>
                </nav>
            </header>
            <Switch>
                <Route exact path="/" component={Arrange}/>
                <Route exact path="/About" component={About}/>
                <section>
                    {login.enableEdit&&
                        <Route exact path="/Movie_Edit" component={MovieListEditor}/>
                    }
                    <Route exact path="/Login" component={Login}/>
                </section>              
            </Switch>
        </Router>
    );
}

const Arrange = () =>{
    return(
        <MovieProvider>
            <Index/>
        </MovieProvider>
      )
}

const Index = () =>{
    
    const [daftarFilm] = useContext(MovieContext)

    return(
        <>
            <section >
            <h1>Daftar Film Film Terbaik</h1>
            {
                daftarFilm !== null && daftarFilm.map((item, index)=>{
                    return(
                        <div id="article-list">
                            <div class="article">
                            <h3>{item.title}</h3>
                            <div style={{display:"inline-block"}}>
                                <div class="conten">
                                    <img src={item.image_url} alt="alt"/>
                                </div>
                                <div class="details">
                                    <h3>Rating {item.rating}</h3>
                                    <h3>Durasi : {Math.round(item.duration/60)} jam</h3>
                                    <h3>Genre : {item.genre}</h3>
                                </div>
                            </div>
                            <p>
                                <strong>Description : </strong>{item.description}
                            </p>
                            </div>
                        </div>
                    )
                })
            }
            </section>
            <footer>
                <h5>copyright &copy; 2020 by Sanbercode</h5>
            </footer>
        </>
    )   
}
