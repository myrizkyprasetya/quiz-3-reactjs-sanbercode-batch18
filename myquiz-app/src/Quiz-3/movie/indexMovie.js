import React from "react"
import {MovieProvider} from "./MovieContext"
import FormMovie from "./FormMovie"
import ListMovie from "./ListMovie"


const MovieListEditor = () =>{
    return(
      <MovieProvider>
          <ListMovie/>
          <br/>
          <FormMovie/>
      </MovieProvider>
    )
  }
  
  export default MovieListEditor
