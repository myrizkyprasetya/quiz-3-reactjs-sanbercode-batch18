import React, { useState,useEffect, createContext } from "react";
import axios from 'axios'

export const MovieContext = createContext();

export const MovieProvider = props => {

    const [daftarFilm, setDaftarFilm] =  useState(null)

    useEffect( () => {
        if (daftarFilm === null){
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDaftarFilm(res.data.map(el=>{ 
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating,
                        image_url: el.image_url
                    }}
                ))
            })
        }
      }, [daftarFilm])
  
    const [input, setInput]  =  useState({title: "", description: "", year: 2020, duration: 120, genre: "",rating: 0, image_url: "", id: null})
  
    return (
      <MovieContext.Provider value={[daftarFilm, setDaftarFilm, input, setInput]}>
        {props.children}
      </MovieContext.Provider>
    );
};
