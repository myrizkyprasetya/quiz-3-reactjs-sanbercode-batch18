import React, {Component} from 'react';
import "./public/css/style.css"

class About extends Component{
    render(){
        return(
            <>
            <div style={{padding: "10px", border: "1px solid #ccc"}}>
                <section>
                <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                <ol>
                    <li><strong style={{width: "100px"}}>Nama:</strong> M. Yasifa Rizky Prasetya</li> 
                    <li><strong style={{width: "100px"}}>Email:</strong> myrp@gmail.com</li> 
                    <li><strong style={{width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 10</li>
                    <li><strong style={{width: "100px"}}>Akun Gitlab:</strong> myrizkyprasetya</li> 
                    <li><strong style={{width: "100px"}}>Akun Telegram:</strong> myrizkyprasetya</li> 
                </ol>
                </section>
            </div>
            <br/>
            <br/>
            <a href="index.js">
            <button>kembali ke index</button>
            </a>
            </>
        );
    }
}
export default About