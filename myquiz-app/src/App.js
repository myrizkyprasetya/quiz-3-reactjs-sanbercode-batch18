import React from 'react';
import './App.css';
import Index from './Quiz-3/index'
import {LoginProvider} from "./Quiz-3/LoginContext";



function App() {
  return (
    <>
      <LoginProvider>
        <Index/>
      </LoginProvider>
    </>
  );
}

export default App;
